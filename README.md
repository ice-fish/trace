# trace

#### 介绍
spring boot demo使用sql server数据库，实现前端页面到控制层、服务接口层、服务实现层、数据库访问层等的实现
欢迎咨询 qq：623763070 vx：liyu6237

#### 软件架构
软件架构说明


#### 安装教程

1、	后续重启服务器时只需要关注服务是否正常运行，已停止时右键启动即可
 
2、	安装部署路径：D:\trace\JAR；
 
3、	项目源文件为：trace-0.0.1-SNAPSHOT.jar,winsw1.exe和winsw1.xml为注册服务使用文件，其余.err和.out为日志文件。
4、	迁移项目时，拷贝trace-0.0.1-SNAPSHOT.jar,winsw1.exe和winsw1.xml，并根据目录结构更新winsw1.xml
 
更新D:\trace\JAR\trace-0.0.1-SNAPSHOT.jar为最新目录结构即可。
5、	然后通过cmd命令提示符在当前目录执行命令：winsw1.exe install
 
6、	通过任务管理器服务界面启动TraceService
 
7、	卸载服务，首先在任务管理器服务界面停止追溯服务；再在命令提示符执行命令：
winsw1.exe uninstall

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

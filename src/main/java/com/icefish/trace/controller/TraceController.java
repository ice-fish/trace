package com.icefish.trace.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.icefish.trace.entity.Booking;
import com.icefish.trace.entity.Trace;
import com.icefish.trace.service.TraceService;
import com.icefish.trace.utils.CodeCommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 追溯控制层
 * @author: liyu
 * @Date:2019/11/4 21:30
 * @param
 * @return
 */
@Controller
public class TraceController {
    @Autowired
    private TraceService traceService;
    @Resource
    private DefaultKaptcha captchaProducer;
    /**
     * 登录验证码SessionKey
     */
    public static final String LOGIN_VALIDATE_CODE = "login_validate_code";
    @GetMapping(value = "trace")
    public String getTraceInfo(@RequestParam("code") String code, Model model){
        Trace trace = traceService.getTraceInfo(code);
        model.addAttribute("trace",trace);
        return "trace";
    }
    @GetMapping(value = "fangwei")
    public String getTracePage(Model model){
        Trace trace = new Trace();
        model.addAttribute("errorMessage","");
        model.addAttribute("traceInfo", trace);
        return "fangWei";
    }
    @PostMapping(value = "trace")
    public String getTraceDetail(@Valid Trace trace, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            List<ObjectError> error = bindingResult.getAllErrors();
            model.addAttribute("errorMessage",error.get(0).getDefaultMessage());
            model.addAttribute("traceInfo",trace);
            return "fangWei";
        }
        Trace trace1 = traceService.getTraceInfo(trace.getTrace_no());
        if (trace1.getGoods_no() == null){
            model.addAttribute("errorMessage","请输入正确的追溯码");
            model.addAttribute("traceInfo",trace);
            return "fangWei";
        }
        model.addAttribute("trace",trace1);
        return "trace";
    }
    /**
    * 驰峰酒业
    * @author LiYu
    * @Date 2019/11/12 0012 15:40
    **/
    @GetMapping(value = "chifeng")
    public String getChiFengInfo(){
        return "chiFengJiuYe";
    }
    /**
    * 自强酒厂
    * @author LiYu
    * @Date 2019/11/12 0012 15:41
    **/
    @GetMapping(value = "ziqiang")
    public String getZiQiangInfo(){
        return "ziQiangJiuChang";
    }
    /**
    * 销售服务
    * @author LiYu
    * @Date 2019/11/12 0012 15:43
    **/
    @GetMapping(value = "shopping")
    public String getShoppingInfo(){
        return "shoppingInfo";
    }

    @GetMapping(value = "booking")
    public String getBookingInfo(Model model){
        Booking booking = new Booking();
        model.addAttribute("errorMessage","");
        model.addAttribute("bookingInfo", booking);
        return "booking";
    }
    @PostMapping(value = "add")
    public String doAddBooking(@Valid Booking booking, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            List<ObjectError> error = bindingResult.getAllErrors();
            model.addAttribute("errorMessage",error.get(0).getDefaultMessage());
            model.addAttribute("bookingInfo",booking);
            return "booking";
        }
        int result = traceService.doAddBooking(booking);
        if (result>0){
            return "success";
        }else {
            return "booking";
        }
    }
    /**
     * 登录验证码图片
     */
    @RequestMapping(value = {"/loginValidateCode"})
    public void loginValidateCode(HttpServletRequest request, HttpServletResponse response) throws Exception{
        CodeCommonUtil.validateCode(request,response,captchaProducer,LOGIN_VALIDATE_CODE);
    }
    /**
     * 检查验证码是否正确
     */
    @RequestMapping("/checkLoginValidateCode")
    @ResponseBody
    public HashMap checkLoginValidateCode(HttpServletRequest request,@RequestParam("validateCode")String validateCode) {
        String loginValidateCode = request.getSession().getAttribute(LOGIN_VALIDATE_CODE).toString();
        HashMap<String,Object> map = new HashMap<String,Object>();
        if(loginValidateCode == null){
            //验证码过期
            map.put("status",null);
        }else if(loginValidateCode.equals(validateCode)){
            //验证码正确
            map.put("status",true);
        }else if(!loginValidateCode.equals(validateCode)){
            //验证码不正确
            map.put("status",false);
        }
        map.put("code",200);
        return map;
    }
}

package com.icefish.trace.dao;

import com.icefish.trace.entity.Trace;
import com.icefish.trace.entity.TraceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TraceMapper {
    long countByExample(TraceExample example);

    int deleteByExample(TraceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Trace record);

    int insertSelective(Trace record);

    List<Trace> selectByExample(TraceExample example);

    Trace selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Trace record, @Param("example") TraceExample example);

    int updateByExample(@Param("record") Trace record, @Param("example") TraceExample example);

    int updateByPrimaryKeySelective(Trace record);

    int updateByPrimaryKey(Trace record);
}
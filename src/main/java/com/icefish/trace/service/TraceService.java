package com.icefish.trace.service;

import com.icefish.trace.entity.Booking;
import com.icefish.trace.entity.Trace;

/**
 * 追溯信息接口层
 * @author: liyu
 * @Date:2019/11/4 21:24
*/
public interface TraceService {
    /**
     * 根据追溯码获取商品信息
     * @author: liyu
     * @Date:2019/11/4 21:25
     * @param traceNo 追溯码
     * @return
     */
    Trace getTraceInfo(String traceNo);
    /**
     * 新增预约
     * @author: liyu
     * @Date:2019/11/12 21:01
     * @param booking 预约信息
     * @return
     */
    int doAddBooking(Booking booking);
}

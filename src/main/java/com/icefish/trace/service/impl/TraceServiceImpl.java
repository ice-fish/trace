package com.icefish.trace.service.impl;

import com.icefish.trace.dao.BookingMapper;
import com.icefish.trace.dao.TraceMapper;
import com.icefish.trace.entity.Booking;
import com.icefish.trace.entity.Trace;
import com.icefish.trace.entity.TraceExample;
import com.icefish.trace.service.TraceService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 追溯信息实现层
 * @author: liyu
 * @Date:2019/11/4 21:26
*/
@Component
public class TraceServiceImpl implements TraceService {
    @Resource
    private TraceMapper traceMapper;
    @Resource
    private BookingMapper bookingMapper;
    @Override
    public Trace getTraceInfo(String traceNo) {
        TraceExample traceExample = new TraceExample();
        TraceExample.Criteria traceCriteria = traceExample.createCriteria();
        traceCriteria.andTrace_noEqualTo(traceNo);
        List<Trace> traceList = traceMapper.selectByExample(traceExample);
        Trace trace = new Trace();
        if (traceList.size()>0){
            trace = traceList.get(0);
        }
        return trace;
    }

    /**
     * 新增预约
     * @author: liyu
     * @Date:2019/11/12 21:02
     * @param booking 预约信息
     * @return
     */
    @Override
    public int doAddBooking(Booking booking) {
        List<Booking> bookingList = bookingMapper.selectByExample(null);
        booking.setBooking_id(bookingList.size()+1);
        int result = bookingMapper.insert(booking);
        return result;
    }
}

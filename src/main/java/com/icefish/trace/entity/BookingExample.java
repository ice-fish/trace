package com.icefish.trace.entity;

import java.util.ArrayList;
import java.util.List;

public class BookingExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BookingExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBooking_idIsNull() {
            addCriterion("booking_id is null");
            return (Criteria) this;
        }

        public Criteria andBooking_idIsNotNull() {
            addCriterion("booking_id is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_idEqualTo(Integer value) {
            addCriterion("booking_id =", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idNotEqualTo(Integer value) {
            addCriterion("booking_id <>", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idGreaterThan(Integer value) {
            addCriterion("booking_id >", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("booking_id >=", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idLessThan(Integer value) {
            addCriterion("booking_id <", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idLessThanOrEqualTo(Integer value) {
            addCriterion("booking_id <=", value, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idIn(List<Integer> values) {
            addCriterion("booking_id in", values, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idNotIn(List<Integer> values) {
            addCriterion("booking_id not in", values, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idBetween(Integer value1, Integer value2) {
            addCriterion("booking_id between", value1, value2, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_idNotBetween(Integer value1, Integer value2) {
            addCriterion("booking_id not between", value1, value2, "booking_id");
            return (Criteria) this;
        }

        public Criteria andBooking_typeIsNull() {
            addCriterion("booking_type is null");
            return (Criteria) this;
        }

        public Criteria andBooking_typeIsNotNull() {
            addCriterion("booking_type is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_typeEqualTo(String value) {
            addCriterion("booking_type =", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeNotEqualTo(String value) {
            addCriterion("booking_type <>", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeGreaterThan(String value) {
            addCriterion("booking_type >", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeGreaterThanOrEqualTo(String value) {
            addCriterion("booking_type >=", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeLessThan(String value) {
            addCriterion("booking_type <", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeLessThanOrEqualTo(String value) {
            addCriterion("booking_type <=", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeLike(String value) {
            addCriterion("booking_type like", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeNotLike(String value) {
            addCriterion("booking_type not like", value, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeIn(List<String> values) {
            addCriterion("booking_type in", values, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeNotIn(List<String> values) {
            addCriterion("booking_type not in", values, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeBetween(String value1, String value2) {
            addCriterion("booking_type between", value1, value2, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_typeNotBetween(String value1, String value2) {
            addCriterion("booking_type not between", value1, value2, "booking_type");
            return (Criteria) this;
        }

        public Criteria andBooking_commentIsNull() {
            addCriterion("booking_comment is null");
            return (Criteria) this;
        }

        public Criteria andBooking_commentIsNotNull() {
            addCriterion("booking_comment is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_commentEqualTo(String value) {
            addCriterion("booking_comment =", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentNotEqualTo(String value) {
            addCriterion("booking_comment <>", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentGreaterThan(String value) {
            addCriterion("booking_comment >", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentGreaterThanOrEqualTo(String value) {
            addCriterion("booking_comment >=", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentLessThan(String value) {
            addCriterion("booking_comment <", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentLessThanOrEqualTo(String value) {
            addCriterion("booking_comment <=", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentLike(String value) {
            addCriterion("booking_comment like", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentNotLike(String value) {
            addCriterion("booking_comment not like", value, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentIn(List<String> values) {
            addCriterion("booking_comment in", values, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentNotIn(List<String> values) {
            addCriterion("booking_comment not in", values, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentBetween(String value1, String value2) {
            addCriterion("booking_comment between", value1, value2, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_commentNotBetween(String value1, String value2) {
            addCriterion("booking_comment not between", value1, value2, "booking_comment");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneIsNull() {
            addCriterion("booking_phone is null");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneIsNotNull() {
            addCriterion("booking_phone is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneEqualTo(String value) {
            addCriterion("booking_phone =", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneNotEqualTo(String value) {
            addCriterion("booking_phone <>", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneGreaterThan(String value) {
            addCriterion("booking_phone >", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneGreaterThanOrEqualTo(String value) {
            addCriterion("booking_phone >=", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneLessThan(String value) {
            addCriterion("booking_phone <", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneLessThanOrEqualTo(String value) {
            addCriterion("booking_phone <=", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneLike(String value) {
            addCriterion("booking_phone like", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneNotLike(String value) {
            addCriterion("booking_phone not like", value, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneIn(List<String> values) {
            addCriterion("booking_phone in", values, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneNotIn(List<String> values) {
            addCriterion("booking_phone not in", values, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneBetween(String value1, String value2) {
            addCriterion("booking_phone between", value1, value2, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_phoneNotBetween(String value1, String value2) {
            addCriterion("booking_phone not between", value1, value2, "booking_phone");
            return (Criteria) this;
        }

        public Criteria andBooking_telIsNull() {
            addCriterion("booking_tel is null");
            return (Criteria) this;
        }

        public Criteria andBooking_telIsNotNull() {
            addCriterion("booking_tel is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_telEqualTo(String value) {
            addCriterion("booking_tel =", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telNotEqualTo(String value) {
            addCriterion("booking_tel <>", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telGreaterThan(String value) {
            addCriterion("booking_tel >", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telGreaterThanOrEqualTo(String value) {
            addCriterion("booking_tel >=", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telLessThan(String value) {
            addCriterion("booking_tel <", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telLessThanOrEqualTo(String value) {
            addCriterion("booking_tel <=", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telLike(String value) {
            addCriterion("booking_tel like", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telNotLike(String value) {
            addCriterion("booking_tel not like", value, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telIn(List<String> values) {
            addCriterion("booking_tel in", values, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telNotIn(List<String> values) {
            addCriterion("booking_tel not in", values, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telBetween(String value1, String value2) {
            addCriterion("booking_tel between", value1, value2, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_telNotBetween(String value1, String value2) {
            addCriterion("booking_tel not between", value1, value2, "booking_tel");
            return (Criteria) this;
        }

        public Criteria andBooking_qqIsNull() {
            addCriterion("booking_qq is null");
            return (Criteria) this;
        }

        public Criteria andBooking_qqIsNotNull() {
            addCriterion("booking_qq is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_qqEqualTo(String value) {
            addCriterion("booking_qq =", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqNotEqualTo(String value) {
            addCriterion("booking_qq <>", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqGreaterThan(String value) {
            addCriterion("booking_qq >", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqGreaterThanOrEqualTo(String value) {
            addCriterion("booking_qq >=", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqLessThan(String value) {
            addCriterion("booking_qq <", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqLessThanOrEqualTo(String value) {
            addCriterion("booking_qq <=", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqLike(String value) {
            addCriterion("booking_qq like", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqNotLike(String value) {
            addCriterion("booking_qq not like", value, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqIn(List<String> values) {
            addCriterion("booking_qq in", values, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqNotIn(List<String> values) {
            addCriterion("booking_qq not in", values, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqBetween(String value1, String value2) {
            addCriterion("booking_qq between", value1, value2, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_qqNotBetween(String value1, String value2) {
            addCriterion("booking_qq not between", value1, value2, "booking_qq");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatIsNull() {
            addCriterion("booking_wechat is null");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatIsNotNull() {
            addCriterion("booking_wechat is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatEqualTo(String value) {
            addCriterion("booking_wechat =", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatNotEqualTo(String value) {
            addCriterion("booking_wechat <>", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatGreaterThan(String value) {
            addCriterion("booking_wechat >", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatGreaterThanOrEqualTo(String value) {
            addCriterion("booking_wechat >=", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatLessThan(String value) {
            addCriterion("booking_wechat <", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatLessThanOrEqualTo(String value) {
            addCriterion("booking_wechat <=", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatLike(String value) {
            addCriterion("booking_wechat like", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatNotLike(String value) {
            addCriterion("booking_wechat not like", value, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatIn(List<String> values) {
            addCriterion("booking_wechat in", values, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatNotIn(List<String> values) {
            addCriterion("booking_wechat not in", values, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatBetween(String value1, String value2) {
            addCriterion("booking_wechat between", value1, value2, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_wechatNotBetween(String value1, String value2) {
            addCriterion("booking_wechat not between", value1, value2, "booking_wechat");
            return (Criteria) this;
        }

        public Criteria andBooking_addressIsNull() {
            addCriterion("booking_address is null");
            return (Criteria) this;
        }

        public Criteria andBooking_addressIsNotNull() {
            addCriterion("booking_address is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_addressEqualTo(String value) {
            addCriterion("booking_address =", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressNotEqualTo(String value) {
            addCriterion("booking_address <>", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressGreaterThan(String value) {
            addCriterion("booking_address >", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressGreaterThanOrEqualTo(String value) {
            addCriterion("booking_address >=", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressLessThan(String value) {
            addCriterion("booking_address <", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressLessThanOrEqualTo(String value) {
            addCriterion("booking_address <=", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressLike(String value) {
            addCriterion("booking_address like", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressNotLike(String value) {
            addCriterion("booking_address not like", value, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressIn(List<String> values) {
            addCriterion("booking_address in", values, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressNotIn(List<String> values) {
            addCriterion("booking_address not in", values, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressBetween(String value1, String value2) {
            addCriterion("booking_address between", value1, value2, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_addressNotBetween(String value1, String value2) {
            addCriterion("booking_address not between", value1, value2, "booking_address");
            return (Criteria) this;
        }

        public Criteria andBooking_priceIsNull() {
            addCriterion("booking_price is null");
            return (Criteria) this;
        }

        public Criteria andBooking_priceIsNotNull() {
            addCriterion("booking_price is not null");
            return (Criteria) this;
        }

        public Criteria andBooking_priceEqualTo(String value) {
            addCriterion("booking_price =", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceNotEqualTo(String value) {
            addCriterion("booking_price <>", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceGreaterThan(String value) {
            addCriterion("booking_price >", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceGreaterThanOrEqualTo(String value) {
            addCriterion("booking_price >=", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceLessThan(String value) {
            addCriterion("booking_price <", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceLessThanOrEqualTo(String value) {
            addCriterion("booking_price <=", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceLike(String value) {
            addCriterion("booking_price like", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceNotLike(String value) {
            addCriterion("booking_price not like", value, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceIn(List<String> values) {
            addCriterion("booking_price in", values, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceNotIn(List<String> values) {
            addCriterion("booking_price not in", values, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceBetween(String value1, String value2) {
            addCriterion("booking_price between", value1, value2, "booking_price");
            return (Criteria) this;
        }

        public Criteria andBooking_priceNotBetween(String value1, String value2) {
            addCriterion("booking_price not between", value1, value2, "booking_price");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
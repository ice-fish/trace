package com.icefish.trace.entity;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

public class Booking {
    private Integer booking_id;

    @NotEmpty(message="活动类型不能为空")
    private String booking_type;

    @NotEmpty(message="客户留言不能为空")
    private String booking_comment;

    @Length(min=11,max=11,message="手机号码必须为11位")
    @NotEmpty(message="手机号码不能为空")
    private String booking_phone;

    private String booking_tel;

    private String booking_qq;

    private String booking_wechat;

    @NotEmpty(message="所在地区不能为空")
    private String booking_address;

    @NotEmpty(message="费用预算不能为空")
    private String booking_price;

    public Integer getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Integer booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_type() {
        return booking_type;
    }

    public void setBooking_type(String booking_type) {
        this.booking_type = booking_type;
    }

    public String getBooking_comment() {
        return booking_comment;
    }

    public void setBooking_comment(String booking_comment) {
        this.booking_comment = booking_comment;
    }

    public String getBooking_phone() {
        return booking_phone;
    }

    public void setBooking_phone(String booking_phone) {
        this.booking_phone = booking_phone;
    }

    public String getBooking_tel() {
        return booking_tel;
    }

    public void setBooking_tel(String booking_tel) {
        this.booking_tel = booking_tel;
    }

    public String getBooking_qq() {
        return booking_qq;
    }

    public void setBooking_qq(String booking_qq) {
        this.booking_qq = booking_qq;
    }

    public String getBooking_wechat() {
        return booking_wechat;
    }

    public void setBooking_wechat(String booking_wechat) {
        this.booking_wechat = booking_wechat;
    }

    public String getBooking_address() {
        return booking_address;
    }

    public void setBooking_address(String booking_address) {
        this.booking_address = booking_address;
    }

    public String getBooking_price() {
        return booking_price;
    }

    public void setBooking_price(String booking_price) {
        this.booking_price = booking_price;
    }
}
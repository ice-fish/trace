package com.icefish.trace.entity;

public class Trace {
    private Integer id;

    private String goods_no;

    private String product_name;

    private String product_time;

    private String product_examiner;

    private String package_time;

    private String package_examiner;

    private String trace_no;

    private String picture_url;

    private String batch;

    private String storage;

    private String bartender;

    private String warehouse;

    private String departure_date;

    private String outgoing_person;

    private String receiving_unit;

    private String brand;

    private String spec;

    private String packing;

    private String volume;

    private String weight;

    private String standard;

    private String production_license;

    private String brand_authorization;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoods_no() {
        return goods_no;
    }

    public void setGoods_no(String goods_no) {
        this.goods_no = goods_no;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_time() {
        return product_time;
    }

    public void setProduct_time(String product_time) {
        this.product_time = product_time;
    }

    public String getProduct_examiner() {
        return product_examiner;
    }

    public void setProduct_examiner(String product_examiner) {
        this.product_examiner = product_examiner;
    }

    public String getPackage_time() {
        return package_time;
    }

    public void setPackage_time(String package_time) {
        this.package_time = package_time;
    }

    public String getPackage_examiner() {
        return package_examiner;
    }

    public void setPackage_examiner(String package_examiner) {
        this.package_examiner = package_examiner;
    }

    public String getTrace_no() {
        return trace_no;
    }

    public void setTrace_no(String trace_no) {
        this.trace_no = trace_no;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getBartender() {
        return bartender;
    }

    public void setBartender(String bartender) {
        this.bartender = bartender;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(String departure_date) {
        this.departure_date = departure_date;
    }

    public String getOutgoing_person() {
        return outgoing_person;
    }

    public void setOutgoing_person(String outgoing_person) {
        this.outgoing_person = outgoing_person;
    }

    public String getReceiving_unit() {
        return receiving_unit;
    }

    public void setReceiving_unit(String receiving_unit) {
        this.receiving_unit = receiving_unit;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getProduction_license() {
        return production_license;
    }

    public void setProduction_license(String production_license) {
        this.production_license = production_license;
    }

    public String getBrand_authorization() {
        return brand_authorization;
    }

    public void setBrand_authorization(String brand_authorization) {
        this.brand_authorization = brand_authorization;
    }
}
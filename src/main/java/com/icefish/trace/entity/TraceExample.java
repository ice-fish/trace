package com.icefish.trace.entity;

import java.util.ArrayList;
import java.util.List;

public class TraceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TraceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGoods_noIsNull() {
            addCriterion("goods_no is null");
            return (Criteria) this;
        }

        public Criteria andGoods_noIsNotNull() {
            addCriterion("goods_no is not null");
            return (Criteria) this;
        }

        public Criteria andGoods_noEqualTo(String value) {
            addCriterion("goods_no =", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noNotEqualTo(String value) {
            addCriterion("goods_no <>", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noGreaterThan(String value) {
            addCriterion("goods_no >", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noGreaterThanOrEqualTo(String value) {
            addCriterion("goods_no >=", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noLessThan(String value) {
            addCriterion("goods_no <", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noLessThanOrEqualTo(String value) {
            addCriterion("goods_no <=", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noLike(String value) {
            addCriterion("goods_no like", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noNotLike(String value) {
            addCriterion("goods_no not like", value, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noIn(List<String> values) {
            addCriterion("goods_no in", values, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noNotIn(List<String> values) {
            addCriterion("goods_no not in", values, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noBetween(String value1, String value2) {
            addCriterion("goods_no between", value1, value2, "goods_no");
            return (Criteria) this;
        }

        public Criteria andGoods_noNotBetween(String value1, String value2) {
            addCriterion("goods_no not between", value1, value2, "goods_no");
            return (Criteria) this;
        }

        public Criteria andProduct_nameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProduct_nameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProduct_nameEqualTo(String value) {
            addCriterion("product_name =", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameGreaterThan(String value) {
            addCriterion("product_name >", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameLessThan(String value) {
            addCriterion("product_name <", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameLike(String value) {
            addCriterion("product_name like", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameNotLike(String value) {
            addCriterion("product_name not like", value, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameIn(List<String> values) {
            addCriterion("product_name in", values, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_nameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "product_name");
            return (Criteria) this;
        }

        public Criteria andProduct_timeIsNull() {
            addCriterion("product_time is null");
            return (Criteria) this;
        }

        public Criteria andProduct_timeIsNotNull() {
            addCriterion("product_time is not null");
            return (Criteria) this;
        }

        public Criteria andProduct_timeEqualTo(String value) {
            addCriterion("product_time =", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeNotEqualTo(String value) {
            addCriterion("product_time <>", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeGreaterThan(String value) {
            addCriterion("product_time >", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeGreaterThanOrEqualTo(String value) {
            addCriterion("product_time >=", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeLessThan(String value) {
            addCriterion("product_time <", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeLessThanOrEqualTo(String value) {
            addCriterion("product_time <=", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeLike(String value) {
            addCriterion("product_time like", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeNotLike(String value) {
            addCriterion("product_time not like", value, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeIn(List<String> values) {
            addCriterion("product_time in", values, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeNotIn(List<String> values) {
            addCriterion("product_time not in", values, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeBetween(String value1, String value2) {
            addCriterion("product_time between", value1, value2, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_timeNotBetween(String value1, String value2) {
            addCriterion("product_time not between", value1, value2, "product_time");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerIsNull() {
            addCriterion("product_examiner is null");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerIsNotNull() {
            addCriterion("product_examiner is not null");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerEqualTo(String value) {
            addCriterion("product_examiner =", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerNotEqualTo(String value) {
            addCriterion("product_examiner <>", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerGreaterThan(String value) {
            addCriterion("product_examiner >", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerGreaterThanOrEqualTo(String value) {
            addCriterion("product_examiner >=", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerLessThan(String value) {
            addCriterion("product_examiner <", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerLessThanOrEqualTo(String value) {
            addCriterion("product_examiner <=", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerLike(String value) {
            addCriterion("product_examiner like", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerNotLike(String value) {
            addCriterion("product_examiner not like", value, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerIn(List<String> values) {
            addCriterion("product_examiner in", values, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerNotIn(List<String> values) {
            addCriterion("product_examiner not in", values, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerBetween(String value1, String value2) {
            addCriterion("product_examiner between", value1, value2, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andProduct_examinerNotBetween(String value1, String value2) {
            addCriterion("product_examiner not between", value1, value2, "product_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_timeIsNull() {
            addCriterion("package_time is null");
            return (Criteria) this;
        }

        public Criteria andPackage_timeIsNotNull() {
            addCriterion("package_time is not null");
            return (Criteria) this;
        }

        public Criteria andPackage_timeEqualTo(String value) {
            addCriterion("package_time =", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeNotEqualTo(String value) {
            addCriterion("package_time <>", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeGreaterThan(String value) {
            addCriterion("package_time >", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeGreaterThanOrEqualTo(String value) {
            addCriterion("package_time >=", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeLessThan(String value) {
            addCriterion("package_time <", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeLessThanOrEqualTo(String value) {
            addCriterion("package_time <=", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeLike(String value) {
            addCriterion("package_time like", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeNotLike(String value) {
            addCriterion("package_time not like", value, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeIn(List<String> values) {
            addCriterion("package_time in", values, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeNotIn(List<String> values) {
            addCriterion("package_time not in", values, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeBetween(String value1, String value2) {
            addCriterion("package_time between", value1, value2, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_timeNotBetween(String value1, String value2) {
            addCriterion("package_time not between", value1, value2, "package_time");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerIsNull() {
            addCriterion("package_examiner is null");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerIsNotNull() {
            addCriterion("package_examiner is not null");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerEqualTo(String value) {
            addCriterion("package_examiner =", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerNotEqualTo(String value) {
            addCriterion("package_examiner <>", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerGreaterThan(String value) {
            addCriterion("package_examiner >", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerGreaterThanOrEqualTo(String value) {
            addCriterion("package_examiner >=", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerLessThan(String value) {
            addCriterion("package_examiner <", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerLessThanOrEqualTo(String value) {
            addCriterion("package_examiner <=", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerLike(String value) {
            addCriterion("package_examiner like", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerNotLike(String value) {
            addCriterion("package_examiner not like", value, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerIn(List<String> values) {
            addCriterion("package_examiner in", values, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerNotIn(List<String> values) {
            addCriterion("package_examiner not in", values, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerBetween(String value1, String value2) {
            addCriterion("package_examiner between", value1, value2, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andPackage_examinerNotBetween(String value1, String value2) {
            addCriterion("package_examiner not between", value1, value2, "package_examiner");
            return (Criteria) this;
        }

        public Criteria andTrace_noIsNull() {
            addCriterion("trace_no is null");
            return (Criteria) this;
        }

        public Criteria andTrace_noIsNotNull() {
            addCriterion("trace_no is not null");
            return (Criteria) this;
        }

        public Criteria andTrace_noEqualTo(String value) {
            addCriterion("trace_no =", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noNotEqualTo(String value) {
            addCriterion("trace_no <>", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noGreaterThan(String value) {
            addCriterion("trace_no >", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noGreaterThanOrEqualTo(String value) {
            addCriterion("trace_no >=", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noLessThan(String value) {
            addCriterion("trace_no <", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noLessThanOrEqualTo(String value) {
            addCriterion("trace_no <=", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noLike(String value) {
            addCriterion("trace_no like", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noNotLike(String value) {
            addCriterion("trace_no not like", value, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noIn(List<String> values) {
            addCriterion("trace_no in", values, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noNotIn(List<String> values) {
            addCriterion("trace_no not in", values, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noBetween(String value1, String value2) {
            addCriterion("trace_no between", value1, value2, "trace_no");
            return (Criteria) this;
        }

        public Criteria andTrace_noNotBetween(String value1, String value2) {
            addCriterion("trace_no not between", value1, value2, "trace_no");
            return (Criteria) this;
        }

        public Criteria andPicture_urlIsNull() {
            addCriterion("picture_url is null");
            return (Criteria) this;
        }

        public Criteria andPicture_urlIsNotNull() {
            addCriterion("picture_url is not null");
            return (Criteria) this;
        }

        public Criteria andPicture_urlEqualTo(String value) {
            addCriterion("picture_url =", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlNotEqualTo(String value) {
            addCriterion("picture_url <>", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlGreaterThan(String value) {
            addCriterion("picture_url >", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlGreaterThanOrEqualTo(String value) {
            addCriterion("picture_url >=", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlLessThan(String value) {
            addCriterion("picture_url <", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlLessThanOrEqualTo(String value) {
            addCriterion("picture_url <=", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlLike(String value) {
            addCriterion("picture_url like", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlNotLike(String value) {
            addCriterion("picture_url not like", value, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlIn(List<String> values) {
            addCriterion("picture_url in", values, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlNotIn(List<String> values) {
            addCriterion("picture_url not in", values, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlBetween(String value1, String value2) {
            addCriterion("picture_url between", value1, value2, "picture_url");
            return (Criteria) this;
        }

        public Criteria andPicture_urlNotBetween(String value1, String value2) {
            addCriterion("picture_url not between", value1, value2, "picture_url");
            return (Criteria) this;
        }

        public Criteria andBatchIsNull() {
            addCriterion("batch is null");
            return (Criteria) this;
        }

        public Criteria andBatchIsNotNull() {
            addCriterion("batch is not null");
            return (Criteria) this;
        }

        public Criteria andBatchEqualTo(String value) {
            addCriterion("batch =", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchNotEqualTo(String value) {
            addCriterion("batch <>", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchGreaterThan(String value) {
            addCriterion("batch >", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchGreaterThanOrEqualTo(String value) {
            addCriterion("batch >=", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchLessThan(String value) {
            addCriterion("batch <", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchLessThanOrEqualTo(String value) {
            addCriterion("batch <=", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchLike(String value) {
            addCriterion("batch like", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchNotLike(String value) {
            addCriterion("batch not like", value, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchIn(List<String> values) {
            addCriterion("batch in", values, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchNotIn(List<String> values) {
            addCriterion("batch not in", values, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchBetween(String value1, String value2) {
            addCriterion("batch between", value1, value2, "batch");
            return (Criteria) this;
        }

        public Criteria andBatchNotBetween(String value1, String value2) {
            addCriterion("batch not between", value1, value2, "batch");
            return (Criteria) this;
        }

        public Criteria andStorageIsNull() {
            addCriterion("storage is null");
            return (Criteria) this;
        }

        public Criteria andStorageIsNotNull() {
            addCriterion("storage is not null");
            return (Criteria) this;
        }

        public Criteria andStorageEqualTo(String value) {
            addCriterion("storage =", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageNotEqualTo(String value) {
            addCriterion("storage <>", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageGreaterThan(String value) {
            addCriterion("storage >", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageGreaterThanOrEqualTo(String value) {
            addCriterion("storage >=", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageLessThan(String value) {
            addCriterion("storage <", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageLessThanOrEqualTo(String value) {
            addCriterion("storage <=", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageLike(String value) {
            addCriterion("storage like", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageNotLike(String value) {
            addCriterion("storage not like", value, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageIn(List<String> values) {
            addCriterion("storage in", values, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageNotIn(List<String> values) {
            addCriterion("storage not in", values, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageBetween(String value1, String value2) {
            addCriterion("storage between", value1, value2, "storage");
            return (Criteria) this;
        }

        public Criteria andStorageNotBetween(String value1, String value2) {
            addCriterion("storage not between", value1, value2, "storage");
            return (Criteria) this;
        }

        public Criteria andBartenderIsNull() {
            addCriterion("bartender is null");
            return (Criteria) this;
        }

        public Criteria andBartenderIsNotNull() {
            addCriterion("bartender is not null");
            return (Criteria) this;
        }

        public Criteria andBartenderEqualTo(String value) {
            addCriterion("bartender =", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderNotEqualTo(String value) {
            addCriterion("bartender <>", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderGreaterThan(String value) {
            addCriterion("bartender >", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderGreaterThanOrEqualTo(String value) {
            addCriterion("bartender >=", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderLessThan(String value) {
            addCriterion("bartender <", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderLessThanOrEqualTo(String value) {
            addCriterion("bartender <=", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderLike(String value) {
            addCriterion("bartender like", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderNotLike(String value) {
            addCriterion("bartender not like", value, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderIn(List<String> values) {
            addCriterion("bartender in", values, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderNotIn(List<String> values) {
            addCriterion("bartender not in", values, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderBetween(String value1, String value2) {
            addCriterion("bartender between", value1, value2, "bartender");
            return (Criteria) this;
        }

        public Criteria andBartenderNotBetween(String value1, String value2) {
            addCriterion("bartender not between", value1, value2, "bartender");
            return (Criteria) this;
        }

        public Criteria andWarehouseIsNull() {
            addCriterion("warehouse is null");
            return (Criteria) this;
        }

        public Criteria andWarehouseIsNotNull() {
            addCriterion("warehouse is not null");
            return (Criteria) this;
        }

        public Criteria andWarehouseEqualTo(String value) {
            addCriterion("warehouse =", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseNotEqualTo(String value) {
            addCriterion("warehouse <>", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseGreaterThan(String value) {
            addCriterion("warehouse >", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseGreaterThanOrEqualTo(String value) {
            addCriterion("warehouse >=", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseLessThan(String value) {
            addCriterion("warehouse <", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseLessThanOrEqualTo(String value) {
            addCriterion("warehouse <=", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseLike(String value) {
            addCriterion("warehouse like", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseNotLike(String value) {
            addCriterion("warehouse not like", value, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseIn(List<String> values) {
            addCriterion("warehouse in", values, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseNotIn(List<String> values) {
            addCriterion("warehouse not in", values, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseBetween(String value1, String value2) {
            addCriterion("warehouse between", value1, value2, "warehouse");
            return (Criteria) this;
        }

        public Criteria andWarehouseNotBetween(String value1, String value2) {
            addCriterion("warehouse not between", value1, value2, "warehouse");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateIsNull() {
            addCriterion("departure_date is null");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateIsNotNull() {
            addCriterion("departure_date is not null");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateEqualTo(String value) {
            addCriterion("departure_date =", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateNotEqualTo(String value) {
            addCriterion("departure_date <>", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateGreaterThan(String value) {
            addCriterion("departure_date >", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateGreaterThanOrEqualTo(String value) {
            addCriterion("departure_date >=", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateLessThan(String value) {
            addCriterion("departure_date <", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateLessThanOrEqualTo(String value) {
            addCriterion("departure_date <=", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateLike(String value) {
            addCriterion("departure_date like", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateNotLike(String value) {
            addCriterion("departure_date not like", value, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateIn(List<String> values) {
            addCriterion("departure_date in", values, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateNotIn(List<String> values) {
            addCriterion("departure_date not in", values, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateBetween(String value1, String value2) {
            addCriterion("departure_date between", value1, value2, "departure_date");
            return (Criteria) this;
        }

        public Criteria andDeparture_dateNotBetween(String value1, String value2) {
            addCriterion("departure_date not between", value1, value2, "departure_date");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personIsNull() {
            addCriterion("outgoing_person is null");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personIsNotNull() {
            addCriterion("outgoing_person is not null");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personEqualTo(String value) {
            addCriterion("outgoing_person =", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personNotEqualTo(String value) {
            addCriterion("outgoing_person <>", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personGreaterThan(String value) {
            addCriterion("outgoing_person >", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personGreaterThanOrEqualTo(String value) {
            addCriterion("outgoing_person >=", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personLessThan(String value) {
            addCriterion("outgoing_person <", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personLessThanOrEqualTo(String value) {
            addCriterion("outgoing_person <=", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personLike(String value) {
            addCriterion("outgoing_person like", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personNotLike(String value) {
            addCriterion("outgoing_person not like", value, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personIn(List<String> values) {
            addCriterion("outgoing_person in", values, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personNotIn(List<String> values) {
            addCriterion("outgoing_person not in", values, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personBetween(String value1, String value2) {
            addCriterion("outgoing_person between", value1, value2, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andOutgoing_personNotBetween(String value1, String value2) {
            addCriterion("outgoing_person not between", value1, value2, "outgoing_person");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitIsNull() {
            addCriterion("receiving_unit is null");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitIsNotNull() {
            addCriterion("receiving_unit is not null");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitEqualTo(String value) {
            addCriterion("receiving_unit =", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitNotEqualTo(String value) {
            addCriterion("receiving_unit <>", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitGreaterThan(String value) {
            addCriterion("receiving_unit >", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitGreaterThanOrEqualTo(String value) {
            addCriterion("receiving_unit >=", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitLessThan(String value) {
            addCriterion("receiving_unit <", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitLessThanOrEqualTo(String value) {
            addCriterion("receiving_unit <=", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitLike(String value) {
            addCriterion("receiving_unit like", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitNotLike(String value) {
            addCriterion("receiving_unit not like", value, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitIn(List<String> values) {
            addCriterion("receiving_unit in", values, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitNotIn(List<String> values) {
            addCriterion("receiving_unit not in", values, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitBetween(String value1, String value2) {
            addCriterion("receiving_unit between", value1, value2, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andReceiving_unitNotBetween(String value1, String value2) {
            addCriterion("receiving_unit not between", value1, value2, "receiving_unit");
            return (Criteria) this;
        }

        public Criteria andBrandIsNull() {
            addCriterion("brand is null");
            return (Criteria) this;
        }

        public Criteria andBrandIsNotNull() {
            addCriterion("brand is not null");
            return (Criteria) this;
        }

        public Criteria andBrandEqualTo(String value) {
            addCriterion("brand =", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandNotEqualTo(String value) {
            addCriterion("brand <>", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandGreaterThan(String value) {
            addCriterion("brand >", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandGreaterThanOrEqualTo(String value) {
            addCriterion("brand >=", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandLessThan(String value) {
            addCriterion("brand <", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandLessThanOrEqualTo(String value) {
            addCriterion("brand <=", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandLike(String value) {
            addCriterion("brand like", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandNotLike(String value) {
            addCriterion("brand not like", value, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandIn(List<String> values) {
            addCriterion("brand in", values, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandNotIn(List<String> values) {
            addCriterion("brand not in", values, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandBetween(String value1, String value2) {
            addCriterion("brand between", value1, value2, "brand");
            return (Criteria) this;
        }

        public Criteria andBrandNotBetween(String value1, String value2) {
            addCriterion("brand not between", value1, value2, "brand");
            return (Criteria) this;
        }

        public Criteria andSpecIsNull() {
            addCriterion("spec is null");
            return (Criteria) this;
        }

        public Criteria andSpecIsNotNull() {
            addCriterion("spec is not null");
            return (Criteria) this;
        }

        public Criteria andSpecEqualTo(String value) {
            addCriterion("spec =", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecNotEqualTo(String value) {
            addCriterion("spec <>", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecGreaterThan(String value) {
            addCriterion("spec >", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecGreaterThanOrEqualTo(String value) {
            addCriterion("spec >=", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecLessThan(String value) {
            addCriterion("spec <", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecLessThanOrEqualTo(String value) {
            addCriterion("spec <=", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecLike(String value) {
            addCriterion("spec like", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecNotLike(String value) {
            addCriterion("spec not like", value, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecIn(List<String> values) {
            addCriterion("spec in", values, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecNotIn(List<String> values) {
            addCriterion("spec not in", values, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecBetween(String value1, String value2) {
            addCriterion("spec between", value1, value2, "spec");
            return (Criteria) this;
        }

        public Criteria andSpecNotBetween(String value1, String value2) {
            addCriterion("spec not between", value1, value2, "spec");
            return (Criteria) this;
        }

        public Criteria andPackingIsNull() {
            addCriterion("packing is null");
            return (Criteria) this;
        }

        public Criteria andPackingIsNotNull() {
            addCriterion("packing is not null");
            return (Criteria) this;
        }

        public Criteria andPackingEqualTo(String value) {
            addCriterion("packing =", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotEqualTo(String value) {
            addCriterion("packing <>", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThan(String value) {
            addCriterion("packing >", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThanOrEqualTo(String value) {
            addCriterion("packing >=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThan(String value) {
            addCriterion("packing <", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThanOrEqualTo(String value) {
            addCriterion("packing <=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLike(String value) {
            addCriterion("packing like", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotLike(String value) {
            addCriterion("packing not like", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingIn(List<String> values) {
            addCriterion("packing in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotIn(List<String> values) {
            addCriterion("packing not in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingBetween(String value1, String value2) {
            addCriterion("packing between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotBetween(String value1, String value2) {
            addCriterion("packing not between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andVolumeIsNull() {
            addCriterion("volume is null");
            return (Criteria) this;
        }

        public Criteria andVolumeIsNotNull() {
            addCriterion("volume is not null");
            return (Criteria) this;
        }

        public Criteria andVolumeEqualTo(String value) {
            addCriterion("volume =", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeNotEqualTo(String value) {
            addCriterion("volume <>", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeGreaterThan(String value) {
            addCriterion("volume >", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeGreaterThanOrEqualTo(String value) {
            addCriterion("volume >=", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeLessThan(String value) {
            addCriterion("volume <", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeLessThanOrEqualTo(String value) {
            addCriterion("volume <=", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeLike(String value) {
            addCriterion("volume like", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeNotLike(String value) {
            addCriterion("volume not like", value, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeIn(List<String> values) {
            addCriterion("volume in", values, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeNotIn(List<String> values) {
            addCriterion("volume not in", values, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeBetween(String value1, String value2) {
            addCriterion("volume between", value1, value2, "volume");
            return (Criteria) this;
        }

        public Criteria andVolumeNotBetween(String value1, String value2) {
            addCriterion("volume not between", value1, value2, "volume");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("weight is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("weight is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(String value) {
            addCriterion("weight =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(String value) {
            addCriterion("weight <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(String value) {
            addCriterion("weight >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(String value) {
            addCriterion("weight >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(String value) {
            addCriterion("weight <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(String value) {
            addCriterion("weight <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLike(String value) {
            addCriterion("weight like", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotLike(String value) {
            addCriterion("weight not like", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<String> values) {
            addCriterion("weight in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<String> values) {
            addCriterion("weight not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(String value1, String value2) {
            addCriterion("weight between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(String value1, String value2) {
            addCriterion("weight not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andStandardIsNull() {
            addCriterion("standard is null");
            return (Criteria) this;
        }

        public Criteria andStandardIsNotNull() {
            addCriterion("standard is not null");
            return (Criteria) this;
        }

        public Criteria andStandardEqualTo(String value) {
            addCriterion("standard =", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotEqualTo(String value) {
            addCriterion("standard <>", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThan(String value) {
            addCriterion("standard >", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardGreaterThanOrEqualTo(String value) {
            addCriterion("standard >=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThan(String value) {
            addCriterion("standard <", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLessThanOrEqualTo(String value) {
            addCriterion("standard <=", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardLike(String value) {
            addCriterion("standard like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotLike(String value) {
            addCriterion("standard not like", value, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardIn(List<String> values) {
            addCriterion("standard in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotIn(List<String> values) {
            addCriterion("standard not in", values, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardBetween(String value1, String value2) {
            addCriterion("standard between", value1, value2, "standard");
            return (Criteria) this;
        }

        public Criteria andStandardNotBetween(String value1, String value2) {
            addCriterion("standard not between", value1, value2, "standard");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseIsNull() {
            addCriterion("production_license is null");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseIsNotNull() {
            addCriterion("production_license is not null");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseEqualTo(String value) {
            addCriterion("production_license =", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseNotEqualTo(String value) {
            addCriterion("production_license <>", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseGreaterThan(String value) {
            addCriterion("production_license >", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseGreaterThanOrEqualTo(String value) {
            addCriterion("production_license >=", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseLessThan(String value) {
            addCriterion("production_license <", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseLessThanOrEqualTo(String value) {
            addCriterion("production_license <=", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseLike(String value) {
            addCriterion("production_license like", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseNotLike(String value) {
            addCriterion("production_license not like", value, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseIn(List<String> values) {
            addCriterion("production_license in", values, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseNotIn(List<String> values) {
            addCriterion("production_license not in", values, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseBetween(String value1, String value2) {
            addCriterion("production_license between", value1, value2, "production_license");
            return (Criteria) this;
        }

        public Criteria andProduction_licenseNotBetween(String value1, String value2) {
            addCriterion("production_license not between", value1, value2, "production_license");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationIsNull() {
            addCriterion("brand_authorization is null");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationIsNotNull() {
            addCriterion("brand_authorization is not null");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationEqualTo(String value) {
            addCriterion("brand_authorization =", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationNotEqualTo(String value) {
            addCriterion("brand_authorization <>", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationGreaterThan(String value) {
            addCriterion("brand_authorization >", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationGreaterThanOrEqualTo(String value) {
            addCriterion("brand_authorization >=", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationLessThan(String value) {
            addCriterion("brand_authorization <", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationLessThanOrEqualTo(String value) {
            addCriterion("brand_authorization <=", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationLike(String value) {
            addCriterion("brand_authorization like", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationNotLike(String value) {
            addCriterion("brand_authorization not like", value, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationIn(List<String> values) {
            addCriterion("brand_authorization in", values, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationNotIn(List<String> values) {
            addCriterion("brand_authorization not in", values, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationBetween(String value1, String value2) {
            addCriterion("brand_authorization between", value1, value2, "brand_authorization");
            return (Criteria) this;
        }

        public Criteria andBrand_authorizationNotBetween(String value1, String value2) {
            addCriterion("brand_authorization not between", value1, value2, "brand_authorization");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}